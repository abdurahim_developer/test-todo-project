from django.contrib import admin
from . import models
# Register your models here.

@admin.register(models.Todo)
class TodoAdmin(admin.ModelAdmin):
    list_display = ['title', 'created', 'updated']
    list_display_links = ['title']