from rest_framework import serializers
from main import models

class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'title',
            'description',
            'created',
            'updated',
        )
        model = models.Todo
