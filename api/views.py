from rest_framework import generics

from main import models
from . import serializers

# Create your views here.

class TodoList(generics.ListAPIView):
    queryset = models.Todo.objects.all()
    serializer_class = serializers.TodoSerializer

class TodoDetail(generics.RetrieveAPIView):
    queryset = models.Todo.objects.all()
    serializer_class = serializers.TodoSerializer

class TodoCreate(generics.CreateAPIView):
    queryset = models.Todo.objects.all()
    serializer_class = serializers.TodoSerializer

class TodoUpdate(generics.RetrieveUpdateAPIView):
    queryset = models.Todo.objects.all()
    serializer_class = serializers.TodoSerializer

class TodoDelete(generics.RetrieveDestroyAPIView):
    queryset = models.Todo.objects.all()
    serializer_class = serializers.TodoSerializer
