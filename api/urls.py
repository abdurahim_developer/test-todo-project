from django.urls import path
from . import views

urlpatterns = [
    path('list', views.TodoList.as_view()),
    path('detail/<int:pk>', views.TodoDetail.as_view()),
    path('create', views.TodoCreate.as_view()),
    path('update/<int:pk>', views.TodoUpdate.as_view()),
    path('delete/<int:pk>', views.TodoDelete.as_view()),
]