# **Django-Rest Todo Project**

### A simple todo app built with django

# **Setup**
The first thing to do is to clone the repository:
`$ git clone https://gitlab.com/abdurahim_developer/test-todo-project.git`
<br>
`$ cd test-todo-project`
----
Create a virtual environment to install dependencies in and activate it:<br />
`$ python -m venv venv`
<br>
`$ source env/bin/activate`
----
Then install the dependencies:<br />
`(venv)$ pip install -r requirements.txt`
----
Then you need to do migrate:<br />
`(venv)$ python manage.py migrate`
----
Then you need to run this project:<br />
`(venv)$ python manage.py runserver`
----
So it has five urls in the project:<br />
<br>
First for create new item  `http://127.0.0.1:8000/api/create`   <br />
<br>
Second for update the available item `http://127.0.0.1:8000/api/update/1` <br />
<br>
Third for delete the available item `http://127.0.0.1:8000/api/delete/1` <br />
<br>
Forth for see the available to-do list `http://127.0.0.1:8000/api/list`     <br />
<br>
Fifth for see the to-do detail `http://127.0.0.1:8000/api/detail/1` <br />